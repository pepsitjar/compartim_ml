**********************************
Tema 1. MACHINE LEARNING AMB ARCGIS
**********************************

1. Classificació d'imatges de satèl·lit utilitzant un algoritme de Machine Learning
====================================================================================

1.1 Introducció
----------------

El procés de classificació d'una imatge raster pot resultar una tasca complexa, que consumeixi molt temps i de la qual resulti complicat extreure'n uns bons resultats.
**Image Classification Wizard** és una eina introduïda a ArcGis Pro que incorpora algoritmes de machine learning per tal de facilitar els procesos de classificació d'imatges raster.

Durem a terme un procés de classificació supervisada d'aquesta imatge de satèl·lit, corresponent a la zona del Lake Pontchartrain a Nova Orleans.

.. figure:: ./_static/open_project_map.png
	:scale: 70%
	:align: center

	**Fig 1.1**: Imatge de satèl·lit a classificar.


1.2 Image Classification Wizard
--------------------------------

- Obrim el projecte i fem clic a la pestanya **Imagery Layer** al panell de continguts d'ArcGis Pro.
Es mostra així el panell **Raster Layer**.

- Fem clic a la pestanya **Data**.


.. figure:: ./_static/data_tab.png
	:scale: 70%
	:align: center

	**Fig 1.2**: Imagery Layer Data Tab.


- Clic a **Classification Wizard** per a obrir el panell per a dur a terme la classificació.

.. figure:: ./_static/configure_page.png
	:scale: 70%
	:align: center

	**Fig 1.3**: Imagery Layer Data Tab.


1.3 Classificació de la imatge
--------------------------------

Per a dur a terme el procés de classificació, escollim els següents paràmetres:

Image Classification Wizard pane
====================================

- **Classification method**: Supervised. Aquest mètode permet a l'usuari decidir la categoria a la qual pertany cadascuna de les classes.
En el mètode no supervisat, l'aplicació s'ocupa de definir les classes en funció de les diferències en les característiques espectrals dels píxels de la imatge.

- **Classification Type**: Object based. Realitza agrupacions de píxels en funció de la seva similitud.

- **Classification Schema**: seleccionem l'schema myNLCDgrey.ecs que es troba al directori *Project > Folders > ImageClassificationWizardTutorial*. A classification schema is used to organize all of the features in your imagery into distinct classes. A schema is saved in an Esri classification schema (.ecs) file, which uses JSON syntax. Schemas are often hierarchical, meaning that you may have a class of forests with sub-classes for evergreen and deciduous.

- **Segmented Image**: Indicar la imatge segmentada. Si no en tenim cap creada, en la pròxima pantalla ens donarà les opcions per a crear-ne una.

- **Training Samples**: àrees d'entrenament. Escollir la capa amb les àrees d'entrenament.

- **Reference Dataset**: zones que s'ha comprovat prèviament la categoría a la qual pertanyen a partir de treball de camp o imatges de major resolució.


Training samples Manager pane
===============================

Des d'aquesta pàgina podem visualitzar les classes definides, segons el model NLCD2011.

.. figure:: ./_static/training_samples_manager_page.png
	:scale: 70%
	:align: center

	**Fig 1.4**: Classes definides, segons el model NLCD2011.

Train page
============

Aquí escollim l'algoritme de classificació.
Usarem el **Support Vector Machine (SVM)** classifier, que és un algoritme basat en ML relativament nou i àmpliament utilitzat per a molts investigadors.

.. figure:: ./_static/train_page.png
	:scale: 70%
	:align: center

	**Fig 1.5**: Algoritme de classificació.




.. important:: El procés tarda uns minuts, mentres s'entrena l'algoritme (trainning classifier...). És un procediment diferent a una classificació supervisada convencional, perquè l'algoritme s'entrena.


https://www.esri.com/arcgis-blog/products/arcgis-pro/imagery/hands-on-experience-with-the-image-classification-wizard-arcgis-pro-1-3/

Explicacio:
https://pro.arcgis.com/en/pro-app/help/analysis/image-analyst/the-image-classification-wizard.htm











La classificació d'imatges consisteix en extreure informació a partir d'una capa raster

Tradicionalmente la teledetección se ha estudiado como una  disciplina complementaria pero  separada de los Sistemas de Información Geográfica. Ello es debido principalmente a que se trata de una materia muy extensa cuyo desarrollo se ha producido en parte de forma ajena al de los SIG. No obstante, a medida que ambos campos se han ido desarrollando, la convergencia entre SIG y teledetección se ha ido haciendo cada vez más evidente. No solo las aplicaciones SIG incorporan  funciones para el manejo, tratamiento y análisis de datos procedentes de la teledetección, sino que las formulaciones de ambos ámbitos contienen elementos similares.

La teledetección es una fuente de datos esencial en los SIG, y el verdadero aprovechamiento de los productos actuales de la teledetección solo se da con el concurso de los SIG y sus capacidades de análisis y manejo de datos.

A lo largo de este módulo descubriréis qué es la teledetección, los principios que rigen la obtención de datos a través de los sensores remotos, las ventajas del uso de imágenes de satélite y su explotación mediante los SIG.



1.2 Definición
----------------

El término teledetección hace referencia a las distintas formas de adquirir información de forma remota a través de sensores sin entrar en contacto físico  con el objeto. Los sensores se localizan en plataformas de observación (bien en aviones o satélites), y registran la información centrada en las regiones del espectro electromagnético. La teledetección tiene como objetivo  obtener las características de la superficie observada y los fenómenos que en ella tienen lugar. Distintas disciplinas se sirven de esta fuente de información (meteorología, oceanografía, geología, geografía,...), por lo que esta nueva forma de ver nuestro planeta viene acompañada por el desarrollo de potentes modelos físico-matemáticos que tratan de describir los procesos físicos que tienen lugar en nuestro entorno.

Casi todo en la teledetección son ventajas. En general podemos decir que su uso permite reducir costes y tiempo en el estudio de los sistemas naturales, estudiar grandes espacios proporcionando una  amplia visión de los hechos geográficos, y obtener una cobertura global y periódica de la superficie terrestre de la mayor parte de la Tierra, incluso en áreas inaccesibles por otros medios. Además, es una tecnología al alcance de todos, gracias al esfuerzo de las distintas agencias espaciales encargadas de la distribución de la información, la cual es cada vez más manejable y homogénea.

Si a todas estas ventajas unimos por un lado, el desarrollo tecnológico (los sistemas de información geográfica, el aumento de la capacidad de memoria y  potencia de los procesadores de los ordenadores personales, el desarrollo de potentes programas de cálculo y de tratamiento de imágenes, los sistemas de posicionamiento, etc.) y por otro lado, el interés creciente de la sociedad en el conocimiento del medio natural (en la búsqueda, gestión, explotación de los recursos naturales, en las interacciones con el hombre, etc.), hacen de la teledetección una interesante fuente de adquisición de datos que hasta hace pocos años resultaba inaccesible para la mayoría de nosotros.


1.3 Un poco de historia en la observación espacial
--------------------------------------------------

La teledetección es una técnica aplicada, y como tal muy dependiente del estado de desarrollo tecnológico existente en cada momento. Dado que conjuga aspectos muy variados (óptica y detectores del sensor, vehículo que lo sustenta, sistemas de transmisión, equipos de tratamiento, etc.), las formas de teledetección han variado en las últimas décadas. El crecimiento ha sido realmente vertiginoso, facilitando una progresión muy notable, tanto en la cantidad, como en la variedad y calidad de la información disponible para campos científicos muy variados.

.. figure:: ./_static/tele1_1.jpg
	:scale: 70%
	:align: center

	**Fig 1.1**: Evolución histórica de los sistemas de teledetección. Fuente: E. Chuvieco.


Las primeras experiencias de teledetección se remontan a 1859, cuando Gaspar Félix obtuvo las primeras fotografías aéreas desde un globo. Al año siguiente, James Wallace repitió la experiencia sobre la ciudad de Boston, poniéndose en evidencia el interés de la nueva perspectiva aérea para un conocimiento más detallado de la organización urbana.

En los años posteriores, se produjo un importante desarrollo de la observación fotográfica desde avión.

Sin embargo, no fue hasta 1956 cuando fueron fotografiadas las primeras vistas de la Tierra desde el espacio, dentro del programa de cohetes de gran altitud de U.S.A. Con los primeros vuelos tripulados del programa Mercury en 1961, los astronautas pudieron observar la tierra a alturas superiores a 1.500 m, y fue entonces cuando se hizo evidente  el gran potencial de la observación espacial como fuente de información y para el análisis de los fenómenos que ocurren sobre la superficie terrestre.. Con los programas Géminis y Apolo se obtuvieron espectaculares fotografías de color y las primeras imágenes multiespectrales, aunque no hubo una observación sistemática ya que el principal objetivo de estos programas era el desarrollo de nuevas tecnologías espaciales.

En los años finales de la década de los 60, la serie de satélites meteorológicos TIROS, suministró imágenes en las zonas visibles e infrarrojo del espectro, de considerable interés para la meteorología (Fig 1.2). Hasta la puesta en órbita a mediados de los 70 de los satélites meteorológicos NOAA, los científicos no comenzaron a obtener información útil de la superficie terrestre.


.. figure:: ./_static/tele1_2.png
	:scale: 50%
	:align: center

	**Fig 1.2**: Primera imagen de la Tierra tomada con el satélite meteorológico TIROS I el 1 de Abril de 1960.

Todas estas experiencias, junto al bagaje aportado por los satélites meteorológicos, hicieron concebir a la NASA proyectos dedicados exclusivamente a la cartografía y evaluación de los recursos naturales. Julio de 1972 supuso la culminación de esta tendencia, con el lanzamiento del primer satélite de la serie **ERTS** (Earth Resources Technollogy Satellite). Este proyecto -conocido como Landsat con la puesta en órbita del segundo satélite en 1975- resulta el más fructífero hasta el momento para aplicaciones civiles de la teledetección.

A la serie Landsat siguieron otros proyectos específicamente diseñados para la observación medio ambiental. Los más conocidos son el laboratorio espacial tripulado Skylab (1973), el satélite oceanográfico Seasat (1978), el de investigación térmica HCMM (1978) -todos ellos propiedad de la NASA-, el satélite francés SPOT (1986), el japonés MOS-1 (1987), el indio IRS-1 (1988), y los rusos Soyuz y Salut.


.. figure:: ./_static/tele1_3.jpg
	:scale: 100%
	:align: center

	**Fig 1.3**: Imagen del satélite ruso Skylab y sello conmemorativo.


La Agencia Espacial Europea y la canadiense ya han lanzado sus propios satélites al espacio, así como la brasileña en colaboración con la china, y varios consorcios privados.




2. Principios físicos de la teledetección
=========================================

En la lección anterior se ha definido la teledetección como aquella técnica que permite obtener información a distancia de los objetos situados sobre la superficie terrestre sin entrar en contacto físico con ellos. Pues bien, para que  esta observación remota sea posible es necesario que entre los objetos y el sensor exista algún tipo de interacción. Veamos un sencillo ejemplo que nos permitirá introducir los tres principales elementos de cualquier **sistema de teledetección: sensor, objeto observado y flujo energético**.

.. important:: Cuando nosotros somos capaces de visualizar un árbol es porque nuestros ojos reciben y traducen convenientemente una energía luminosa procedente del mismo. Esa señal, además, no es originada por el árbol, sino por un foco energético exterior que lo ilumina. De ahí que no seamos capaces de percibir ese árbol en plena oscuridad.

Según este ejemplo, el sensor sería nuestro *ojo*, el objeto observado el *árbol*, y el flujo energético que permite poner ambos en relación la *luz solar*.

Veamos con mas detalle los elementos del proceso de teledetección, que se representan de forma esquemática en la siguiente imagen:

.. figure:: ./_static/tele2_1.png
	:scale: 100%
	:align: center

	**Fig 2.1**: Esquema de un sistema de teledetección.

a) **Fuente de radiación**. Puede ser de origen *natural* o *artificial*. La primera de ellas es la forma más importante de teledetección, pues se deriva directamente de la luz solar, principal fuente de energía de nuestro planeta. El sol ilumina la superficie terrestre, que refleja esa energía en función del tipo de cubierta presente sobre ella. Ese flujo reflejado se recoge por el sensor, que lo transmite posteriormente a las estaciones receptoras.
De igual forma, la observación remota puede basarse en la energía emitida por las propias cubiertas, o en la que podríamos enviar desde un sensor que fuera capaz tanto de generar su propio flujo energético como de recoger posteriormente su reflexión sobre la superficie terrestre.

b) **Objetos** que interaccionan con la radiación o la emiten.

c) **Atmósfera** por la que se desplaza la radiación, tanto desde la fuente hasta el objeto como desde el objeto hasta el receptor. La atmósfera también interactúa con la radiación, introduciendo igualmente perturbaciones en ella.

d) **Receptor**, que recoge la radiación una vez ésta ha sido perturbada o emitida por los objetos. El receptor va a generar como producto final una imagen (en términos de un SIG, una capa raster), en cuyas celdas se va a contener un valor que indica la intensidad de la radiación. Estos valores son valores enteros que indican el nivel de dicha radiación dentro de una escala definida (habitualmente valores entre 1 y 256), y se conocen dentro del ámbito de la teledetección como *Niveles Digitales*.[1]_





2.1 Principios físicos de la radiación
----------------------------------------

Conocer los conceptos fundamentales de la radiación y su interacción con la materia es fundamental para entender cómo, utilizando la radiación de una fuente dada, se crea una imagen como resultado final en un proceso de teledetección.


2.1.1 La radiación electromagnética
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La **radiación electromagnética** es una de las cuatro fuerzas fundamentales de la naturaleza,[2]_ y deriva del campo electromagnético, el cual es ejercido por las partículas cargadas electromagnéticamente.

Un cuerpo con carga, en reposo o que se mueve en el espacio, genera un campo electromagnético definido mediante dos vectores: el campo eléctrico y el campo magnético. Se define la radiación electromagnética como una combinación de campos eléctricos y magnéticos oscilantes y perpendiculares entre sí que se propagan a través del espacio transportando energía de un lado a otro.

A diferencia de otro tipos de onda como el sonido, que necesita un medio material para propagarse, la radiación electromagnética se puede propagar en el vacío. Se puede visualizar la radiación electromagnética como dos campos que se generan mútuamente, por eso no necesitan ningún medio material para propagarse.

.. figure:: ./_static/tele2_2.png
	:scale: 100%
	:align: center

	**Fig 2.2**: Propagación de los campos eléctrico y magnético. Las ondas vibran perpendicularmente a la dirección del movimiento; los campos eléctrico y magnético forman entre sí un ángulo recto, y viajan a la velocidad de la luz.

Según la teoría ondulatoria, las ondas electromagnéticas se desplazan a la velocidad de la luz siguiendo un modelo armónico y continuo, y se pueden describir por dos elementos: **longitud de onda** (λ) y **frecuencia** (F). La primera hace referencia a la distancia entre dos picos sucesivos de una onda, mientras que la frecuencia designa el número de ciclos pasando por un punto fijo en una unidad de tiempo. Ambos elementos están inversamente relacionados (una mayor longitud de onda -y por tanto, una menor frecuencia- tiene asociada una menor energía de la radiación):

c= λF

donde **c** indica la velocidad de la luz (3x10 :sup:`8` m/s), **λ** expresa la longitud de onda (habitualmente en micrómetros), y **F** la frecuencia (Hertzios, ciclos por segundo).


.. figure:: ./_static/tele2_3.jpg
	:scale: 100%
	:align: center

	**Fig 2.3**: Esquema de onda electromagnética.


Ésto implica que la radiación en longitudes de onda largas son más difíciles de detectar que aquellas provenientes de longitudes cortas, de ahí que las primeras requieran medios de detección más refinados. Por ejemplo, para que un sensor detecte las microondas (con longitudes de onda grandes, y por tanto contenido energético bajo) tiene que permanecer fijo sobre un sitio durante un período de tiempo relativamente largo, conocido como tiempo de demora ('*dwell time*') para la captación de una adecuada cantidad de radiación. Otro modo de realizar esta captación es aumentando el área observada para aumentar la cantidad de energía que recibe el sensor.

2.1.2 El espectro electromagnético
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

De lo mencionado hasta ahora se deduce que puede definirse cualquier tipo de energía radiante en función de su longitud de onda o frecuencia. Aunque la sucesión de valores de longitud de onda es continua, suelen establecerse una serie de bandas en donde la radiación electromagnética manifiesta un comportamiento similar. La organización de estas bandas de longitudes de onda o frecuencia se denomina espectro electromagnético, y comprende desde las longitudes de onda más cortas (rayos gamma, rayos X), hasta las Kilométricas. Las unidades de medida más comunes se relacionan con la longitud de onda. Para las más cortas se utilizan micrómetros, mientras las más largas se miden en centímetros o metros.

En la siguiente imagen se muestra un esquema del espectro electromagnético y sus principales regiones de interés:

.. figure:: ./_static/tele2_4.png
	:scale: 100%
	:align: center

	**Fig 2.4**: Espectro electromagnético y sus principales regiones de interés. Fuente: Wikipedia.

Desde el punto de vista de la teledetección, conviene destacar una serie de bandas espectrales que son las más frecuentemente utilizadas por los sensores actuales. A continuación se detallan las distintas regiones electromagnéticas del espectro, sus características generales y su uso potencial:

**Ultravioleta:**

La porción ultravioleta (UV) del espectro contiene radiación más allá de la porción violeta de las longitudes de onda visibles. La radiación en este rango se compone  de longitudes de onda cortas (0,300 para 0,446 µm) y alta frecuencia. Las longitudes de onda UV son usadas en aplicaciones geológicas y atmosféricas. Algunas rocas y minerales emiten luz visible en presencia de radiación UV. La florescencia asociada con hidrocarburos naturales son útiles en la búsqueda de campos petrolíferos en el mar. En la alta atmósfera, el ozono (O :sub:`3`) absorbe la luz ultravioleta, por lo que su uso sirve para estudiar los cambios en la capa de ozono.

**Espectro visible:**

Se denomina así por tratarse de la única radiación electromagnética que pueden percibir nuestros ojos coincidiendo con las longitudes de onda en donde es máxima la radiación solar. Suelen distinguirse tres bandas elementales, que se denominan azul (0,4 a 0,5 µm); verde (0,5 a 0,6 µm), y rojo (0,6 a 0,7 µm), en razón de los colores primarios que nuestros ojos perciben a esas longitudes de onda.

.. figure:: ./_static/tele2_5.jpg
	:scale: 50%
	:align: center

	**Fig 2.5**: Longitudes de onda en el visible.

La luz visible detectada principalmente por los sensores depende de las características de reflexión de la superficie de objetos. Entre los ejemplos de aplicaciones actuales que usan el rango visible del espectro electromagnético están la identificación de rasgos urbanos, la discriminación del terreno y la vegetación, el estudio de la productividad del océano, el estudio de la cobertura nubosa, de la precipitación, de la nieve, y de la cubierta de hielo.

.. figure:: ./_static/tele2_6.jpg
	:scale: 30%
	:align: center

	**Fig 2.6**: Imagen en visible que muestra el avance del desierto sobre un núcleo urbano.


**Infrarrojo:**

La porción que sigue al rango visible del espectro, es la región infrarroja (IR). La región infrarroja, se extiende desde aproximadamente 0,7 hasta 100 µm, lo que significa que es más de 100 veces mayor que la porción visible.
El infrarrojo, a su vez, se divide en **Infrarrojo próximo, Infrarrojo medio e Infrarrojo lejano o térmico**.

El infrarrojo próximo (0,7 a 1,3µm), que se denomina también infrarrojo reflejado o fotográfico, resulta de especial importancia por su capacidad para discriminar masas vegetales y concentraciones de humedad.

El infrarrojo medio (1,3 a 8µm) resulta idóneo para estimar el contenido de humedad en la vegetación y en la detección de focos de alta temperatura.


El infrarrojo lejano o térmico (8 a 100µm), que incluye la porción emisiva del espectro terrestre, en donde se detecta el calor proveniente de la mayor parte de las cubiertas terrestres.

.. figure:: ./_static/tele2_7.jpg
	:scale: 30%
	:align: center

	**Fig 2.7**: Imagen infrarroja del litoral Catalán y el Golfo de León. En azul están representadas las aguas más frías, en rojo las más calientes. La tierra está enmascarada y aparece en blanco.

**Microondas:**

La porción del espectro correspondiente a las microondas, tienen longitudes de onda mayores que el infrarrojo, que se extienden entre 1 µm para 1 m. Las radiaciones del microondas incluye un rango amplio de longitudes de onda. Las longitudes más cortas tienen propiedades parecidas a la radiación térmica del IR, mientras que las más largas mantienen propiedades parecidas a las destinadas a transmitir ondas de radio. Las microondas se usan en el estudio del océano, de los ríos, de la atmósfera, de la geología, de la agricultura, del hielo, e incluso para estudios topográficos. Como la emisión de microondas está influenciada por el contenido de humedad, es útil para representar mapas de humedad del terreno, hielo del mar, corrientes marinas, y vientos en la superficie. Otras aplicaciones incluyen detección de capas de aceite, medidas del perfil de ozono atmosférico y vapor de agua.

.. figure:: ./_static/tele2_8.jpg
	:scale: 30%
	:align: center

	**Fig 2.8**: Imagen SAR (Radar de Apertura Sintética) correspondiente al microondas. La imagen muestra el vertido del Prestige en las costas gallegas.


2.2 Interacción entre la radiación y los objetos
-------------------------------------------------

La radiación emitida por una fuente de radiación es alterada por la presencia de los objetos, que interactúan con ella. Independientemente de su procedencia, para toda radiación se dan tres fenómenos fundamentales al alcanzar un objeto:

**Absorción**. Ocurre cuando la radiación que penetra en una superficie es incorporada en la estructura molecular del objeto. Todos los objetos absorben algo de radiación incidente. La radiación absorbida puede ser emitida más tarde hacia la atmósfera.

**Transmisión**. Ocurre cuando la radiación atraviesa material y pasa al otro lado del objeto. La transmisión juega un papel menor en la interacción de la energía con el objetivo a observar y está en función de las características del objeto.

**Reflexión**. La reflexión ocurre cuando la radiación ni es absorbida ni transmitida. La reflexión de la energía depende de las propiedades del objeto y de la rugosidad de la superficie, variable según sea la longitud de onda de la radiación incidente.

.. figure:: ./_static/tele2_9.png
	:scale: 100%
	:align: center

	**Fig 2.9**: Absorción, reflexión y transmisión.

Estos tres fenómenos se dan en diferente proporción en función de las características del objeto y de la radiación. Para una longitud de onda dada, existe pues un porcentaje de la radiación que es absorbida por el objeto, otra que se transmite a través de él, y otra que es reflejada. La parte que interesa a efectos de la teledetección es la que posteriormente puede recogerse y emplearse para la generación de las imágenes.
La proporción en la que los tres procesos anteriores se dan en un objeto no es la misma para todas las radiaciones. Un objeto puede absorber gran parte de la radiación dentro de una región del espectro y, sin embargo, reflejar la mayoría de ella en una región distinta. Es por ello que en función del análisis que se desee realizar, debe trabajarse con imágenes que traten una u otra región.

Igualmente, una imagen con varias bandas contiene información sobre la intensidad de la radiación reflejada en distintas partes del espectro. Puesto que cada objeto refleja de forma diferente la radiación en cada una de esas partes, pueden igualmente emplearse para identificar objetos particulares si se conoce la respuesta de éstos en determinadas bandas. Aparece de este modo el concepto de *firma espectral* como la respuesta característica de un tipo de objeto dentro del espectro electromagnético. En el siguiente apartado lo expondremos con más detalle.

Además de la interacción con los objetos que se pretenden estudiar, la radiación interactúa con la atmósfera, afectando al resultado y siendo una variable a considerar en ciertas operaciones posteriores con las imágenes. Veremos en el capítulo 2.4 de la lección los efectos que producen dichas interacciones, y en la unidad tres del módulo se analizará cómo corregir posibles errores derivados de ellas.

2.2.1 La firma espectral
^^^^^^^^^^^^^^^^^^^^^^^^

La **firma espectral** es el conjunto de reflectancias en las distintas longitudes de onda que presenta un objeto o material dado. Dicho de otro modo, la firma espectral es la 'huella dactilar' que caracteriza a cada objeto, y que deriva directamente de sus propiedades y de cómo éstas influyen en la forma en que dicho objeto refleja la radiación incidente.

Es gracias a la información que proporcionan las firmas espectrales que es posible identificar en una imagen de satélite la naturaleza de los objetos.

Las firmas espectrales son a menudo representadas gráficamente en un eje de coordenadas, donde la longitud de onda está en el eje de las abscisas, y la reflectancia en el eje de las ordenadas.

A partir de medidas de laboratorio, se han obtenido unas curvas de reflectividad espectral para las principales cubiertas terrestres. Como puede observarse, algunas tienden a presentar una respuesta uniforme en distintas longitudes de onda, mientras otras ofrecen un comportamiento mucho más selectivo.

.. figure:: ./_static/tele2_10.png
	:scale: 100%
	:align: center

	**Fig 2.10**: Firmas espectrales típicas de distintas cubiertas. Fuente: Elaboración propia.

La nieve presenta una reflectividad alta y constante, pues refleja la mayor parte de la energía incidente a distintas longitudes de onda. Por el contrario, el agua absorbe la mayor parte de la energía que recibe, cuanto más nos situamos en longitudes de onda mayores. Por su parte, la vegetación presenta un comportamiento muy cromático, con bajos valores de reflectividad en el espectro visible, más elevados en el infrarrojo cercano y menores en el medio.

A continuación vamos a ver con más detalle algunas de las características espectrales básicas de la vegetación, el terreno, la nieve y el agua:

La reflectancia espectral de vegetación verde es baja en la porción visible del espectro a causa de la absorción de clorofila. En el IR cercano es más alta debido a la estructura celular de la planta. En el IR medio decrece de nuevo, debido al contenido en agua de las células.

.. figure:: ./_static/tele2_11.png
	:scale: 80%
	:align: center

	**Fig 2.11**: Reflectancia espectral de la vegetación sana. Fuente: Elaboración propia.

La reflectancia espectral del suelo aumenta su longitud de onda en la porción visible del espectro y luego queda constante en el IR cercano (‘near-IR’) y el infrarrojo de onda corta (‘SWIR’), con algunos descensos locales debido a la absorción de agua a 1.4 y 1.9 µm y debido a la absorción de la arcilla a 1.4 y 2.2 µm.

.. figure:: ./_static/tele2_12.png
	:scale: 80%
	:align: center

	**Fig 2.12**: Reflectancia espectral del suelo. Fuente: Elaboración propia.

La reflectancia espectral de la nieve es alta en el visible y en el IR cercano (‘Near IR’), y baja en el IR de onda corta (‘SWIR’).
La reflectancia espectral de agua clara es baja en todas las porciones del espectro. Cuando el agua tiene materiales suspendidos se observa un aumento de  la reflectancia en la porción visible del espectro.

.. figure:: ./_static/tele2_13.png
	:scale: 80%
	:align: center

	**Fig 2.13**: Reflectancia espectral de la nieve. Fuente: Elaboración propia.

Las regiones espectrales que serán más útiles en teledetección serán aquellas en las que puedan distinguirse las firmas espectrales que representan las características de la superficie.

.. figure:: ./_static/tele2_10.png
	:scale: 80%
	:align: center

	**Fig 2.14**: Firmas espectrales típicas de distintas cubiertas. Fuente: Elaboración propia.


Como muestra la figura anterior, las longitudes de onda correspondientes al rojo del visible pueden servir para separar el suelo y la vegetación. En la región del IR cercano (‘Near IR’) (de 0.7 a 2.5 µm), los tres tipos están bien definidos: la vegetación tiene una reflectancia más alta que el suelo. En IR de onda corta (‘SWIR’), la situación se invierte y es el suelo el que muestra reflectancias más altas que la vegetación. Las regiones espectrales donde se producen estos cruces en los índices de reflectancia son muy útiles para separar estos  tipos, y es donde se suelen situar las bandas de observación de los sensores espaciales.

Existen bibliotecas de materiales conocidos que resultan muy útiles para determinar la región espectral en la que las características de una superficie están bien definidas. Dado que la curva de reflectancia de un material es una propiedad invariable, los espectros registrados en las bibliotecas espectrales deben corresponder a los mismos materiales en otras localizaciones. Ésto permite la identificación y clasificación de los píxeles de una imagen en base a sus propiedades espectrales. Entre los recursos de la lección hemos puesto a vuestra disposición algunas de las principales bibliotecas de firmas espectrales.


2.3 Factores que modifican la signatura espectral
--------------------------------------------------

El flujo de energía recibido por el sensor no sólo depende de la reflectividad de la cubierta, sino también de una serie de factores externos que modifican lo que podríamos llamar su comportamiento espectral teórico. Veamos algunos de estos factores:

a) Ángulo de iluminación solar, muy dependiente de la fecha del año y del momento de paso del satélite.

b) Modificaciones que el relieve introduce en el ángulo de iluminación: orientación de las laderas o pendiente.

c) Influencia de la atmósfera, especialmente en lo que se refiere a la absorción por nubes y a la dispersión selectiva en distintas longitudes de onda.

d) Variaciones medio ambientales en la cubierta: asociación con otras superficies, homogeneidad que presenta, estado fenológico, …

e) Sustrato edafológico o litológico, especialmente influyente cuando la cubierta observada presenta una densidad media.

Todos estos factores expresan la complejidad que conlleva la caracterización de un fenómeno a partir de imágenes de satélite. También debe servirnos para relativizar la validez de las firmas espectrales indicadas en el capítulo anterior, que no deben considerarse más que como reflectividades de referencia.

.. important:: En definitiva, los distintos fenómenos de la superficie terrestre no tienen un comportamiento espectral único y permanente, que coincida con sus curvas de reflectividad espectral y permita reconocerlos sin confusión frente a otros. En torno a un comportamiento tipo, denominado firma espectral, cada fenómeno presenta una cierta variabilidad espectral que dificulta su discriminación de otras superficies.

.. figure:: ./_static/tele2_15.png
	:scale: 100%
	:align: center

	**Fig 2.15**: Factores que modifican la firma espectral: a) variaciones estacionales de la altura solar; b) condiciones atmosféricas; c) orientación; d) condición fenológica; e) pendiente; f) sustrato edáfico.

2.4 Interacción de la energía electromagnética con la Atmósfera
----------------------------------------------------------------

La energía que recibe un sensor desde un objetivo de interés debe pasar a través de la atmósfera. Los componentes gaseosos y las partículas de materia[3]_ dentro de la atmósfera pueden afectar la intensidad y la distribución espectral de la energía y pueden impedir la observación de características de la superficie.

.. figure:: ./_static/tele2_16.png
	:scale: 100%
	:align: center

	**Fig 2.16**: Influencia de la atmósfera en la radiación.


Los mecanismos de **absorción** de la atmósfera dependientes de la frecuencia, alteran la cantidad de radiación solar que alcanza la superficie de la Tierra. Otro efecto atmosférico que también puede alterar significativamente la radiación que llega al sensor es la **dispersión** o difusión de radiación por partículas de la atmósfera.

La suma de estas dos formas de pérdida de energía se denominan *atenuación atmosférica*. Ambas perturbaciones varían en sus efectos de una región espectral a otra. Por regla general, el fenómeno de dispersión añade intensidad a la señal recibida por el sensor, mientras que la absorción atmosférica reduce el nivel de las medidas espectrales realizadas.

2.4.1 Dispersión atmosférica
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La dispersión es el resultado de la interacción entre la radiación electromagnética y las partículas o moléculas de gas presentes en la atmósfera. Estas partículas tienen un tamaño muy variable y redireccionan la radiación incidente, desviándola de su camino. Sus efectos pueden ser muy irregulares y pueden variar rápidamente en el tiempo, de ahí a la considerable dificultad que implica realizar un análisis cuantitativo de este fenómeno.

Los principales causantes de la dispersión atmosférica son los aerosoles y el vapor de agua. Los aerosoles son partículas en suspensión de origen muy diverso: oceánico -debidas al movimiento de las aguas-, o continental, polvo en suspensión o partículas emitidas por combustión.

Los procesos de dispersión son muy complejos y difíciles de cuantificar en las imágenes de satélite. Normalmente no se dispone de datos coetáneos a la adquisición de éstas, por lo que la corrección atmosférica se basa en relaciones entre elementos de la propia imagen. Este proceso de corrección se trabajará con más detalle en la unidad tres del módulo.

2.4.2 Dispersión atmosférica
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La absorción es el segundo proceso que interacciona con la radiación que llega a la atmósfera. Gases tales como el dióxido de carbono, el vapor de agua y el ozono tienen bandas de absorción en regiones particulares del espectro electromagnético.

Como consecuencia de ello, la observación espacial se reduce a determinadas bandas del espectro, conocidas como ventana atmosféricas, en donde la transmisividad de la atmósfera es suficientemente alta.

Las principales ventanas atmosféricas son las siguientes:

a) espectro visible e infrarrojo cercano, situada entre 0,3 y 1,35µm.

b) en el infrarrojo medio: 1,5 a 1,8µm ; 2 a 2,4µm ; 2,9 a 4,2 µm y 4,5 a 5,5µm .

c) infrarrojo térmico: entre 8 y 14 µm.

d) microondas: por encima de 20 µm, en donde la atmósfera es prácticamente transparente.


Estas ventanas atmosféricas son idóneas para realizar procesos de teledetección, por lo que el diseño de los sensores espaciales tiende a ajustarse a estas bandas, evitando interferencias extrañas al fenómeno que pretende observarse. Por ello, no resulta muy habitual encontrar este problema en la interpretación de imágenes (salvo en caso de cobertura nubosa, normalmente evitables gracias a una buena selección de la imagen).

Si se pretende, por el contrario, observar la atmósfera en lugar de la superficie terrestre, los sectores espectrales más convenientes son, precisamente, aquellos en donde la absorción atmosférica es alta. Por esta razón, los satélites meteorológicos incorporan bandas en estas regiones del espectro.

2.5 Detección de la energía por el sensor
-----------------------------------------

La señal emitida por la superficie observada sufre dos tipos de modificaciones antes de ser registrada por el sensor: las **perturbaciones atmosféricas** y las **perturbaciones instrumentales**.

Las perturbaciones atmosféricas pueden ser **geométricas** y **radiométricas**. Las perturbaciones geométricas son debidas al hecho que la radiación electromagnética no se propaga en línea recta a través de la atmósfera al variar la densidad atmosférica con la altura. Las perturbaciones radiométricas (comentadas en el apartado anterior) son debidas a las interacciones electromagnéticas con los componentes atmosféricos, los cuales pueden producir una atenuación de la señal (por absorción molecular y difusión atmosférica) o una aportación indeseada de la misma (variable en función de la composición de la atmósfera).

Las perturbaciones instrumentales dependen del tipo de receptor, del tipo de medida y de las condiciones de la medida (inestabilidad de la plataforma, observación oblicua, etc.) y se traducen en una función del aparato y en una ecuación del calibrado que tiene en cuenta dichas perturbaciones. Para extraer las informaciones que nos interesan a partir de la señal registrada tendremos que realizar un proceso inverso al de su modificación por las causas antes mencionadas, ésto es:


- la **deconvolución instrumental**, que requiere conocer la función de calibrado del sensor utilizado y realizar las correcciones geométricas. Podemos obtener la radiancia a nivel del sensor.

- la **deconvolución atmosférica**, de la cual obtendremos la radiancia procedente de la superficie del suelo. Para ello debemos conocer los parámetros atmosféricos necesarios para evaluar la transmisividad y la radiancia atmosférica.

Conocida la radiancia procedente del medio objeto de estudio, podremos determinar sus parámetros ópticos (emisividad, reflectividad), los cuales están relacionados con los parámetros físicos (turbidez, temperatura, rugosidad).

.. important:: Así pues, el primer paso para el análisis de los datos adquiridos mediante sensores remotos es convertir la señal de salida del instrumento en radiancia. La caracterización de la relación existente entre la radiancia detectada por el sensor y la señal de salida del mismo es lo que se denomina calibración radiométrica del sensor.

Los sensores normalmente generan señales analógicas de voltaje o frecuencia, las cuales son transformadas antes de la transmisión desde el satélite a la Tierra en señales digitales, por las ventajas de detección de errores, almacenamiento y procesamiento que presentan. Dependiendo de la sensibilidad del sensor, la velocidad de muestreo y el coste de la transmisión de datos, las señales se codifican con mayor o menor número de bits.

Lo usual es usar 8 bits (de 0 a 255 niveles digitales o DN), aunque por ejemplo, el Advanced Very High Resolution Radiometer (AVHRR), con una resolución radiométrica de 0,12K a 300K, usa 10 bits (1024 DN).
Para relacionar los ND con la radiancia, hay que tener en cuenta todos los aspectos del flujo de información, desde que la señal exterior entra en el sensor como radiación hasta que la señal analógica o digital se recibe en la Tierra como ND. Ésto incluye la conversión de la señal recibida por el sensor en pulso eléctrico, la conversión de esa respuesta eléctrica en un valor digital (el ND) -con su correspondiente error de digitalización- y la transmisión de la información en una determinada frecuencia a la estación receptora en la Tierra.

Una vez en la estación receptora, y conociendo los valores máximo y mínimo de la radiancia, puede calcularse el valor de la radiancia para cada uno de los píxeles que integra la imagen.

Hay que tener en cuenta que el equipo que se encarga de hacer funcionar todo ese flujo de información a bordo de un satélite trabaja en un ambiente completamente hostil, sometido a unas condiciones gravitatorias, de presión y de temperatura extremas, muy diferentes a las del laboratorio en que se calibró. Estas condiciones pueden afectar al funcionamiento del sensor, por lo que debe ser comprobado y calibrado tanto en tierra como en órbita de forma periódica.

La cantidad de energía total medida por el sensor (medida a través de una diferencia de potencial o voltaje), depende de las características espectrales y de los filtros usados en el sensor. Mediante la calibración geofísica, relacionamos esta energía medida con el sensor con la radiancia espectral correspondiente a la anchura de banda observada del sensor teniendo en cuenta la respuesta espectral del mismo.

Normalmente, los sensores son calibrados exponiéndolos al menos a dos niveles conocidos de radiancia espectral y calculando con ello unos coeficientes propios de cada sensor. Estos coeficientes se miden en la Tierra antes del lanzamiento del satélite, y después, a bordo del satélite en el espacio.[4]_ Una vez en órbita, también suelen realizarse medidas simultáneas de la radiancia desde el satélite y desde la Tierra sobre blancos conocidos.

Referencias
-----------

Atmospheric correction:
http://grass.osgeo.org/wiki/Atmospheric_correction

Conghe S.; Curtis E.; Karen C.; Mary P. and Scott A. 2000. Classification and Change Detection Using Landsat TM Data: When and How to Correct Atmospheric Effects?

Chuvieco, E. Fundamentos de la Teledetección espacial. Ed. Arial Ciencia. 3ª ed.

Chuvieco, E. 2008. Teledetección Ambiental. La observación de la Tierra desde el espacio. Ed. Arial Ciencia. 3a ed. Barcelona

Campbell, J.B. 1987. Introduction to Remote Sensing. The Guilford Press. New York

CCRS, 2009. Fundamentals of Remote Sensing. Canada Center for Remote Sensing.

Horning, N.; Robinson, J. et al. 2010. Remote Sensing for Ecology and Conservation. A Handbook of Techniques. Oxford University Press.

Olave-Solar, C; Santana, A; Butrovic, N; Paola, A.  (2008) Thermal Variability in the northeastern area of brunswick peninsula, using landsat data. Anales del Instituto de la Patagonia.

Olaya, V. 2011. Libro Libre SIG. Fuentes Principales de datos espaciales.

Sarah C. Goslee; (2011). Analysing Remote Sensing Data in R: The landsat Package. Journal of Sataistical Software.

Short, N. 2009. Remote Sensing Tutorial. NASA.



.. [1] El nombre (Nivel Digital) se justifica por tratarse de un valor numérico, no visual, pero que puede fácilmente traducirse a una intensidad visual o, si se prefiere, a un nivel de gris, mediante cualquier convertidor digital-analógico.
.. [2] Las otras tres son la gravitatoria, la interacción nuclear débil y la interacción nuclear fuerte.
.. [3] La composicion volumétrica de la atmósfera es de un 78% de nitrógeno (N2), un 21% de oxígeno (02), un 0,9% de argón (Ar), un 0,03% de dióxido de carbono (CO2) y trazas de otros gases nobles y ozono (O3). Además, se registra la presencia de pequeñas cantidades de vapor de agua (H2O) y de diversos aerosoles.
.. [4] Los datos proporcionados por los dos primeros se incluyen normalmente en los manuales de cada sensor o como cabeceras de información en las cintas en las que se graban las imágenes. Las medidas correspondientes al tercer grupo se realizan mediante el empleo de radiómetros de campo que simulan desde cerca del objeto las condiciones espectrales de adquisición del sensor. Es importante comprender cómo se organizan estos datos pues el primer problema con el que uno se encuentra cuando inicia el tratamiento de los mismos consiste en leer la información de la imagen, la cual puede interpretarse de manera equívoca si no se leen bien los parámetros de calibración que le acompañan.
